package interfaces;


import java.util.Arrays;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.extensions.MatlabNumericArray;
import matlabcontrol.extensions.MatlabTypeConverter;


/**
 *http://code.google.com/p/matlabcontrol/wiki/Walkthrough
 *http://code.google.com/p/matlabcontrol/
 *
 *alternative : https://fr.mathworks.com/help/matlab/matlab_external/execute-matlab-functions-from-java.html
 */

public class MatlabControl {

		public static void main(String[] args) throws MatlabConnectionException, MatlabInvocationException
		{
		    //Create a proxy, which we will use to control MATLAB
		    MatlabProxyFactory factory = new MatlabProxyFactory();
		    MatlabProxy proxy = factory.getProxy();

		    ////////////////EVAL
		    
		    //Display 'hello world' just like when using the demo
		    proxy.eval("disp('hello world')");

		    //////////////FEVAL
		    
		    //Display 'hello world' like before, but this time using feval
		    proxy.feval("disp", "hello world");
		    
		    proxy.feval("clear");
		    
		    ////////////////VARIABLE SET/GET
		    	    
		    //Set a variable, add to it, retrieve it, and print the result
		    proxy.setVariable("a", 5);
		    proxy.eval("a = a + 6");
		    double result = ((double[]) proxy.getVariable("a"))[0];
		    System.out.println("Result: " + result);

		    //////////////ARRAY GETTING
		    
		    MatlabTypeConverter processor = new MatlabTypeConverter(proxy);
		    
		    //Create a 4x3x2 array filled with random values
		    proxy.eval("array = randn(4,3,2)");

		    //Print a value of the array into the MATLAB Command Window
		    proxy.eval("disp(['entry: ' num2str(array(3, 2, 1))])");

		    //Get the array from MATLAB
		    MatlabNumericArray mlarray = processor.getNumericArray("array");
		    
		    //Print out the same entry, using Java's 0-based indexing
		    System.out.println("entry: " + mlarray.getRealValue(2, 1, 0));
		    
		    //Convert to a Java array and print the same value again    
		    double[][][] javaArray = mlarray.getRealArray3D();
		    System.out.println("entry: " + javaArray[2][1][0]);


		    //////////////ARRAY SENDING	    
		    
		    //Create and print a 2D double array
		    double[][] matrix = new double[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		    System.out.println("Original: ");
		    for(int i = 0; i < matrix.length; i++)
		    {
		        System.out.println(Arrays.toString(matrix[i]));
		    }
		        
		    //Send the array to MATLAB, transpose it, then retrieve it and convert it to a 2D double array
		    processor.setNumericArray("array", new MatlabNumericArray(matrix, null));
		    proxy.eval("array = transpose(array)");
		    double[][] transposedArray = processor.getNumericArray("array").getRealArray2D();
		        
		     //Print the returned array, now transposed
		     System.out.println("Transposed: ");
		     for(int i = 0; i < transposedArray.length; i++)
		     {
		         System.out.println(Arrays.toString(transposedArray[i]));
		     }

			    //////////////GETTING RETURN VALUES

		     //Create an array for this example
		     proxy.eval("array = magic(3)");

		     //Invoke eval, specifying 1 argument to be returned - arguments are returned as an array
		     Object[] returnArguments = proxy.returningEval("array(2,2)", 1);
		     //Retrieve the first (and only) element from the returned arguments
		     Object firstArgument = returnArguments[0];
		     //Like before, cast and index to retrieve the double value
		     double innerValue = ((double[]) firstArgument)[0];
		     //Print the result
		     System.out.println("Result (long): " + innerValue);

		     //Or all in one step
		     double val = ((double[]) proxy.returningEval("array(2,2)", 1)[0])[0];
		     System.out.println("Result (short): " + val);

			    //////////////GETTING RETURN VALUES with FEVAL

		     //By specifying 3 return arguments, returns as String arrays the loaded M-files, MEX files, and Java classes
		     Object[] inmem = proxy.returningFeval("inmem", 3);
		     System.out.println("Java classes loaded:");
		     System.out.println(Arrays.toString((String[]) inmem[2]));
		         
		     //Retrieve MATLAB's release date by providing the -date argument
		     Object[] releaseDate = proxy.returningFeval("version", 1, "-date");
		     System.out.println("MATLAB Release Date: " + releaseDate[0]);


		    //Disconnect the proxy from MATLAB
		    proxy.disconnect();
		}


}
