package fileManipulations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic class for file access/modification
 * 
 * @author Cédric Herpson
 * 
 */
public class FileManipulations {

	/************************************
	 * 
	 * WRITE (in) a FILE
	 * 
	 ***********************************/

	/**
	 * Add to the end of a file fileName, at the location location, information
	 * contained in obj
	 * 
	 * @param location
	 *            if the repositories (and/or the file) do not exists, they are
	 *            automatically created.
	 * @param obj
	 *            should implement the toString method.
	 * @param fileName
	 * @param add
	 *            true --> add to the end of the file, false --> to the
	 *            beginning
	 */
	public static void writeInfo(final String location, final Object obj,
			final String fileName, final boolean add) {
		File rep;
		File f;

		rep = new File(location);
		if (!rep.exists()) {
			rep.mkdirs(); // s permit to create all the path,not only the final file or directory
		}

		f = new File(location + fileName + ".data");
		try {
			// System.out.println("open in modification");
			final FileWriter f1 = new FileWriter(f, add);// modification, ajout
															// en fin

			// System.out.println("print the absolut path of the file");
			// System.out.println(f.getAbsolutePath());

			f1.write("" + obj.toString() + "\n");
			f1.close();
			// System.out.println("FERMETURE Modification");

		} catch (final Exception e) {
			System.out
					.println("Error during the openning (in writing) or closing of the file");
		}

	}

	/************************************
	 * 
	 * READ in a FILE
	 * 
	 ***********************************/

	/**
	 * 
	 * @param filePath path to the file, name of the file included (with extension)
	 * @return An instance of {@link BufferedReader} that allow you to process the file line by line
	 * 
	 * Path p= Paths.get("src/main/resources/map2015-config.txt");
	 * 
	 * A bug seems to appear when the reader.readline is not called in the same function; the stream seems to be cut, but i dont understand why
	 */
	public static BufferedReader readFile(Path filePath) {

		try (BufferedReader reader = Files.newBufferedReader(filePath,StandardCharsets.UTF_8)) {
			return reader;
			
			//String line = null;
			//while ((line = reader.readLine()) != null) {
			//	System.out.println(line);
			//}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
		return null;
	}
	
	/**
	 * Read a line in an already opened file fr
	 * 
	 * @param fr
	 * @return The extracted line as one string
	 * @throws IOException
	 */
	public static String readLineInFile(final FileReader fr) throws IOException {
		String s = "";
		char c = (char) fr.read();
		while (c == '\n') {
			c = (char) fr.read();
		}
		while (c != '\n' & c != '\r') {
			s = s + c;
			// System.out.println("s: "+s);
			c = (char) fr.read();
		}
		return s;
	}

	/**
	 * Read a float from a fileReader object; cope with both "." and ","
	 * @param fr
	 * @return the associated float
	 * @throws IOException
	 */
	public static float readFloatFromFile(final FileReader fr)
			throws IOException {
		float i = -1;
		boolean marqueur = false;
		float f = 0;
		int c = fr.read();
		// System.out.println("c: "+c);
		if (c == -1) {
			return 0;
		}
		while (!(c >= '0' & c <= '9') & c != '-') {
			c = fr.read();
		}
		if (c == '-') {
			marqueur = true;
			c = fr.read();
		}
		while (c != '.' & c != ',') {
			f = 10 * f + (c - 48);
			c = fr.read();
		}
		if (c == '.') {
			c = fr.read();
			while (c >= '0' & c <= '9') {// (c!=',' & c!='\n'){
				f = f + (c - 48) * (float) Math.pow(10, i);
				c = fr.read();
				i--;
			}
		}
		if (marqueur) {
			f = -f;
		}
		// System.out.println("End of the reading: "+f);
		return f;
	}

	/************************************
	 * 
	 * FILE MANIPULATIONS
	 * 
	 ***********************************/

	/**
	 * 
	 * @param directoryPath
	 * @param ext
	 * @return the list of all the names of the files of the directory
	 *         directoryPath whose extension is ext
	 */
	public static List<String> getDirectoryContent(final String directoryPath,
			final String ext) {

		final String[] temporaryListOfFileName = new File(directoryPath).list();
		final List<String> listOfFileName = new ArrayList<String>();

		int j = 0;
		while (j < temporaryListOfFileName.length) {
			if (temporaryListOfFileName[j].endsWith(ext)) {
				listOfFileName.add(temporaryListOfFileName[j]);
			}
			j++;
		}
		return listOfFileName;
	}

	/**
	 * Copy sourceFile to destFile
	 * 
	 * @param sourceFile
	 *            absolute path
	 * @param destFile
	 *            absolute path
	 * @return return true if ok, false otherwise
	 */
	public static boolean copyFile(final String sourceFile,
			final String destFile) {

		FileChannel in = null; // canal d'entrée
		FileChannel out = null; // canal de sortie

		try {
			// Init
			in = new FileInputStream(sourceFile).getChannel();
			out = new FileOutputStream(destFile).getChannel();

			// Copie depuis le in vers le out
			in.transferTo(0, in.size(), out);
		} catch (final Exception e) {
			e.printStackTrace(); // n'importe quelle exception
		} finally { // finalement on ferme, quoi qu'il arrive
			if (in != null) {
				try {
					in.close();
				} catch (final IOException e) {
					return false;
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (final IOException e) {
					return false;
				}
			}
		}
		return true;
	}

}
