package fileManipulations;

import java.io.File;
import java.io.FileOutputStream;
//
//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.input.SAXBuilder;
//import org.jdom.output.Format;
//import org.jdom.output.XMLOutputter;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import debug.Debug;

/**
 * This class contain simple generic methods used to work with xml files
 * 
 * @author Cédric Herpson
 * 
 */
public class XmlUtil {

	// We begin our document with the root
	static Element root;

	// We create a new JDOM document based on the root we just built
	static Document document;

	/**************************************************************************
	 * 
	 * General methods to print screen and write into a file an Xml structure
	 * 
	 **************************************************************************/

	/**
	 * Load in memory the file given in argument
	 * 
	 * @param filePath
	 *            complete path, extension included
	 * @return the root node (Element) or null if file not found
	 */
	public static Element LoadXmlFile(final String filePath) {

		// verify that the network exist
		final File f = new File(filePath);
		if (!f.exists()) {
			Debug.info("Xml.LoadXmlFile, file not found: " + filePath, 3);
			return null;
		} else {
			if (!filePath.endsWith(".xml")) {
				Debug.info("Xml.LoadXmlFile, not xml file (!ending by .xml): "
						+ filePath, 3);
				return null;
			} else {
				// We create a SAXBuilder instance
				final SAXBuilder sxb = new SAXBuilder();
				try {
					// We create a new JDOM document with the xml target file as
					// argument
					XmlUtil.document = sxb.build(new File(filePath));
				} catch (final Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}

				// We initialise the root with the document's root
				XmlUtil.root = XmlUtil.document.getRootElement();
				// Debug.info("name of the root: "+root.getName(),3);
				// Debug.info("Name of the map extracted from the tree: "+root.getChild("name").getText(),
				// 3);

				return XmlUtil.root;
			}
		}
	}

	/**
	 * Add the subtree at the end of the file filename located in directory
	 * 
	 * @param filename
	 *            name of the file to open (.xml included)
	 * @param directory
	 *            path of the file (must finish with/)
	 * @param subTreeRootNode
	 *            subtree to add at the end of the xml file
	 */
	public static void addEndXmlFich(final String filename,
			final String directory, final Element subTreeRootNode) {
		final Element root = XmlUtil.LoadXmlFile(directory + filename);
		// org.jdom.Document document= new Document(root);
		root.addContent((Element) subTreeRootNode.clone());
		XmlUtil.WriteXml(directory + filename, root.getDocument());
	}

	/**
	 * Create the file filename.xml in the location directory
	 * 
	 * @param filename
	 *            name of the responsible element (the target, e.g the
	 *            container)
	 * @param directory
	 *            the path of the directory where the file must be saved (finish
	 *            with /)
	 * @param treeRootNode the xml tree to add
	 * @return the filename with extension
	 */
	public static String createXmlFich(final String filename,
			final String directory, final Element treeRootNode) {
		final Element root = new Element("Struct");
		final Document document = new Document(root);

		// ident of the target
		final Element ident = new Element("name");
		ident.setText(filename);
		root.addContent(ident);

		root.addContent(treeRootNode);
		XmlUtil.WriteXml(directory + filename + ".xml", document);

		return filename + ".xml";
	}

	/******************************
	 * PRIVATE METHODS
	 *****************************/

	/**
	 * ShowXml print the xml file previously loaded or built in memory
	 * 
	 */
	private static void ShowXml() {
		try {
			System.out.println("ShowXml");
			// We use a standard read-out with getPrettyFormat()
			final XMLOutputter exit = new XMLOutputter(Format.getPrettyFormat());
			exit.output(XmlUtil.document, System.out);
		} catch (final java.io.IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Write the document given in parameter in the file fichier
	 * 
	 * @param fichier
	 *            (complete path, name and extension, ex: c:/maps/mapName.xml)
	 * @param document
	 */
	private static void WriteXml(final String fichier,final Document document) {
		try {
			final XMLOutputter exit = new XMLOutputter(Format.getPrettyFormat());
			exit.output(document, new FileOutputStream(fichier));
		} catch (final java.io.IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**********************************************************************************
	 * 
	 * Methods for the access, read and write SensorNetworks
	 * 
	 *********************************************************************************/

}

/**
 * 
 /****************************************************************************
 * ******
 * 
 * Methods for the access, read and write of the logs
 * 
 *********************************************************************************
 * 
 * /** Write the environment topology in the file FileName located by its path
 * directory
 * 
 * @param directory
 * @param fileName
 * @param topo
 * 
 *            public static void WriteEnvXml(String directory,String
 *            fileName,Topologie topo){ root = new Element("map"); document= new
 *            Document(root);
 * 
 *            //ident of the map Element ident = new Element("name");
 *            ident.setText(fileName); root.addContent(ident);
 * 
 *            Element size = new Element("size");
 *            size.setText(""+topo.getSizeX()); root.addContent(size);
 * 
 *            Element maxAltitude = new Element("maxAltitude");
 *            maxAltitude.setText(""+topo.getMaxAlt());
 *            root.addContent(maxAltitude);
 * 
 *            Element visionLength= new Element("visionLength");
 *            visionLength.setText(""+topo.getVisionLength());
 *            root.addContent(visionLength);
 * 
 *            for (int i=0;i<topo.size();i++){ Element square = new
 *            Element("square");
 * 
 *            Square sq=topo.get(i);
 * 
 *            //ident of the square Attribute id = new
 *            Attribute("id",""+sq.getId()); square.setAttribute(id);
 * 
 *            //state of the state Element state = new Element("state");
 *            state.setText(""+sq.isVisited()); square.addContent(state);
 * 
 * 
 *            //altitude of the square Element altitude= new
 *            Element("altitude"); altitude.setText(""+sq.getAltitude());
 *            square.addContent(altitude);
 * 
 *            // properties of the square Element properties=new
 *            Element("properties"); square.addContent(properties);
 * 
 *            Set<Square.PropertiesType> ensProp =sq.getSetPropType();
 * 
 *            if (ensProp.isEmpty()){ Element awardValue = new
 *            Element("awardValue");
 *            awardValue.setText(""+Square.DefaultValue.awardValue.getValue());
 *            properties.addContent(awardValue);
 * 
 *            Element strategicInterest = new Element("strategicInterest");
 *            strategicInterest
 *            .setText(""+Square.DefaultValue.strategicInterest.getValue());
 *            properties.addContent(strategicInterest); }else{ Iterator
 *            iter=ensProp.iterator(); while (iter.hasNext()){
 *            Square.PropertiesType pt=(Square.PropertiesType)iter.next();
 *            Element temp= new Element(pt.name());
 *            temp.setText(""+sq.getPropertyValue(pt));
 *            properties.addContent(temp); }
 * 
 *            }
 * 
 *            // Object(s) on the square Element obj=new Element("object");
 *            square.addContent(obj);
 * 
 *            Set<Square.ObjectType> ensObj =sq.getSetObjType(); Iterator
 *            iter1=ensObj.iterator(); while (iter1.hasNext()){
 *            Square.ObjectType ot=(Square.ObjectType)iter1.next(); Element
 *            temp1= new Element(ot.name()); obj.addContent(temp1); }
 * 
 *            root.addContent(square); } // Print result //ShowXml();
 * 
 *            // Write the file WriteXml(directory+"/"+fileName);
 * 
 *            }
 * 
 * 
 *            /** Load in memory the Environment file fileName located by its
 *            path directory.
 * 
 * @param directory
 * @param fileName
 * @return the topology of the environment, null if the file does not exist
 * 
 *         public static Topologie ReadEnvXml(String directory,String fileName){
 * 
 *         //verify that the map exist File f=new File(directory+"/"+fileName);
 *         if (!f.exists()){ return null; }else{
 * 
 *         // empty topology Topologie topo=new Topologie();
 * 
 *         // We create a SAXBuilder instance SAXBuilder sxb = new SAXBuilder();
 *         try { // We create a new JDOM document with the xml target file as
 *         argument document = sxb.build(new File(directory+"/"+fileName)); }
 *         catch(Exception e){ e.printStackTrace(); System.exit(-1); }
 * 
 * 
 * 
 *         // We initialise the root with the document's root root =
 *         document.getRootElement();
 *         Debug.info("name of the root: "+root.getName(),1);
 *         Debug.info("Name of the map extracted from the tree: "
 *         +root.getChild("name").getText(), 1);
 *         topo.setName(root.getChild("name").getText());
 * 
 *         // Size of the map
 *         Debug.info("Size of the map : "+root.getChild("size").getText(), 1);
 *         topo
 *         .setSize(Integer.parseInt(root.getChild("size").getText()),Integer
 *         .parseInt(root.getChild("size").getText()));
 * 
 *         // Maximum altitude of the map
 *         Debug.info("Maximum altitude : "+root.getChild
 *         ("maxAltitude").getText(), 1);
 *         topo.setMaxAlt(Integer.parseInt(root.getChild
 *         ("maxAltitude").getText()));
 * 
 *         // Vision length of the agents on the map
 *         Debug.info("Vision length defined : "
 *         +root.getChildText("visionLength"), 1);
 *         topo.setVisionLength(Integer.parseInt
 *         (root.getChildText("visionLength")));
 * 
 *         // We create a list with all the square node of the map List
 *         squareList = root.getChildren("square");
 * 
 *         // We visit all square Iterator iter = squareList.iterator();
 *         while(iter.hasNext()){
 * 
 *         Element current = (Element)iter.next();
 * 
 *         // We get the current square Id int
 *         currentId=Integer.parseInt(current.getAttributeValue("id"));
 *         Debug.info("Current square Id : "+currentId,1);
 * 
 *         // We get the current square state boolean squareState =
 *         Boolean.parseBoolean(current.getChild("state").getText());
 *         Debug.info("Current square State (know ?): "+squareState,1);
 * 
 *         if (!squareState){ // The square is not know for now,
 *         topo.addSquare(new Square(currentId)); }else{
 * 
 *         // We get the current square altitude int
 *         currentAltitude=Integer.parseInt
 *         (current.getChild("altitude").getText());
 *         Debug.info("Current square altitude : "+currentAltitude,1);
 * 
 *         Square temp=new Square(currentId,currentAltitude);
 *         //topo.addSquare(temp);
 * 
 * 
 *         // MOVE TO THE PROPETIES NODE OF THE CURRENT SQUARE Element prop =
 *         current.getChild("properties");
 * 
 *         // get all the child of the properties node List
 *         listElem=prop.getChildren(); Iterator iter1=listElem.iterator();
 *         while(iter1.hasNext()){ Element current1=(Element)iter1.next();
 *         Debug.
 *         info("Attribute name : "+current1.getName()+" content: "+current1
 *         .getText(),1);
 * 
 *         temp.setPropertyValue(PropertiesType.valueOf(current1.getName()),
 *         Float.parseFloat(current1.getText())); }
 * 
 * 
 *         // MOVE TO THE OBJECT NODE OF THE CURRENT SQUARE Element
 *         obj=current.getChild("object");
 * 
 *         // get all the child of the object node List
 *         listObj=obj.getChildren(); Iterator iter2=listObj.iterator();
 *         while(iter2.hasNext()){ Element current2=(Element)iter2.next();
 * 
 *         Debug.info("attribute name : "+current2.getName()+" content: "+
 *         current2.getText(),1); try{
 *         temp.addObj(ObjectType.valueOf(current2.getName
 *         ()),Class.forName(current2.getText()).newInstance());
 *         }catch(Exception e){
 *         System.out.println("Exception dans liaison xml nom/classe pour objets "
 *         ); e.printStackTrace(); System.exit(-1); }
 * 
 *         }// endWhile on object topo.addSquare(temp); }// endElse }//endWhile
 *         on square
 * 
 *         return topo; } }
 * 
 * 
 *         /********************************************************************
 *         **************
 * 
 *         Methods for the access (read and write) of the situations
 * 
 **********************************************************************************/

/**
 * Write the specifics observations of an agent into his memory
 * 
 * 
 * 
 * public static void WriteSituXml(String directory,Situation s,int
 * situationId,int nbStep){
 * 
 * Debug.info(
 * "DO NOT FORGET TO ASSOCIATE THE RULE NUMBER THAT HAVE VALIDATE THE PREMISE OF THE see(x,y)-->shot(x,y) RULES"
 * ,3);
 * 
 * //1� build the Xml
 * 
 * root = new Element("agent.situation"); document= new Document(root);
 * 
 * //ident of the agent.situation
 * 
 * Element ident = new Element("name"); ident.setText(""+situationId);
 * root.addContent(ident);
 * 
 * // nb step in the episode before ending Element step= new Element("nbStep");
 * step.setText(""+nbStep); root.addContent(step);
 * 
 * //award Element reward= new Element("reward");
 * reward.setText(""+s.getRewardValue()); root.addContent(reward);
 * 
 * // value of potential interest Element vip= new Element("vip");
 * vip.setText(""+s.getVip()); root.addContent(vip);
 * 
 * //orientation Element orientation=new Element("orientation");
 * orientation.setText(""+s.getOrientation()); root.addContent(orientation);
 * 
 * //lastAction Element lastAction=new Element("lastAction");
 * lastAction.setText(""+s.getLastAction()); root.addContent(lastAction);
 * 
 * 
 * // add the list of concepts Element concepts=new Element("concepts");
 * 
 * List<String> ensConcept =s.getConceptList(); Iterator
 * iter=ensConcept.iterator(); int i=0; while (iter.hasNext()){ Element temp=
 * new Element("c "+i); temp.setText((String)iter.next());
 * concepts.addContent(temp); } root.addContent(concepts);
 * 
 * //add the list of present objects Element objects=new Element("objects");
 * 
 * Set<Square.ObjectType> ensObjects=s.getPresentObjects();
 * iter=ensObjects.iterator(); i=0; while(iter.hasNext()){ Element temp= new
 * Element("o "+i); temp.setText(((Square.ObjectType)iter.next()).getValue());
 * concepts.addContent(temp); } root.addContent(objects);
 * 
 * 
 * Element caracs=new Element("caracteristics");
 * 
 * Element agAltitude=new Element("agentAltitude");
 * agAltitude.setText(""+s.getAgentAltitude()); caracs.addContent(agAltitude);
 * 
 * Element minAlt=new Element("minAltitude");
 * minAlt.setText(""+s.getMinAltitude()); caracs.addContent(minAlt);
 * 
 * Element maxAlt=new Element("maxAltitude");
 * maxAlt.setText(""+s.getMaxAltitude()); caracs.addContent(maxAlt);
 * 
 * Element meanAlt=new Element("meanAlt");
 * meanAlt.setText(""+s.getMeanAltitude()); caracs.addContent(meanAlt);
 * 
 * Element extent_fieldOfVision=new Element("extent_fieldOfVision");
 * extent_fieldOfVision.setText(""+s.getExtent_fieldOfVision());
 * caracs.addContent(extent_fieldOfVision);
 * 
 * Element theoricalMaximum_visionLength=new
 * Element("theoricalMaximum_visionLength");
 * theoricalMaximum_visionLength.setText
 * (""+s.getTheoricalMaximum_visionLength());
 * caracs.addContent(theoricalMaximum_visionLength);
 * 
 * Element maximum_visionLength=new Element("maximum_visionLength");
 * maximum_visionLength.setText(""+s.getMaximum_visionLength());
 * caracs.addContent(maximum_visionLength);
 * 
 * Element altitudeHomogeneity=new Element("altitudeHomogeneity");
 * altitudeHomogeneity.setText(""+s.getAltitudeHomogeneity());
 * caracs.addContent(altitudeHomogeneity);
 * 
 * Element meanMct=new Element("meanMct"); meanMct.setText(""+s.getMeanMct());
 * caracs.addContent(meanMct);
 * 
 * Element roundAngle=new Element("roundAngle");
 * roundAngle.setText(""+s.getRoundAngle()); caracs.addContent(roundAngle);
 * 
 * Element realAngle=new Element("realAngle");
 * realAngle.setText(""+s.getRealAngle()); caracs.addContent(realAngle);
 * 
 * root.addContent(caracs);
 * 
 * /************************************************************************
 * //add the situations squares List<PersSquare> listPersSquare =
 * s.getPersonnalRepresentation();
 * 
 * for (int w=0;w<listPersSquare.size();w++){ Element persSquare = new
 * Element("persSquare");
 * 
 * PersSquare psq=listPersSquare.get(w);
 * 
 * //ident of the square Attribute id = new Attribute("id",""+psq.getId());
 * persSquare.setAttribute(id);
 * 
 * //visionLength Element visionLength = new Element("visionLength");
 * visionLength.setText(""+psq.getVisionLength());
 * persSquare.addContent(visionLength);
 * 
 * //sizeX,sizeY Element sizeX = new Element("sizeX");
 * sizeX.setText(""+psq.getSizeX()); persSquare.addContent(sizeX);
 * 
 * Element sizeY = new Element("sizeY"); sizeY.setText(""+psq.getSizeY());
 * persSquare.addContent(sizeY);
 * 
 * //state of the state Element state = new Element("state");
 * state.setText(""+psq.isVisited()); persSquare.addContent(state);
 * 
 * //altitude of the square Element altitude= new Element("altitude");
 * altitude.setText(""+psq.getAltitude()); persSquare.addContent(altitude);
 * 
 * //Mct value Element mct=new Element("mct"); mct.setText(""+psq.getMct());
 * persSquare.addContent(mct);
 * 
 * // properties of the square Element properties=new Element("properties");
 * persSquare.addContent(properties);
 * 
 * Set<Square.PropertiesType> ensProp =psq.getSetPropType();
 * 
 * iter=ensProp.iterator(); while (iter.hasNext()){ Square.PropertiesType
 * pt=(Square.PropertiesType)iter.next(); Element temp= new Element(pt.name());
 * temp.setText(""+psq.getPropertyValue(pt)); properties.addContent(temp); }
 * 
 * // Object on the square Element obj=new Element("object");
 * persSquare.addContent(obj);
 * 
 * Set<Square.ObjectType> ensObj =psq.getSetObjType(); Iterator
 * iter1=ensObj.iterator(); while (iter1.hasNext()){ Square.ObjectType
 * ot=(Square.ObjectType)iter1.next(); Element temp1= new Element(ot.name());
 * obj.addContent(temp1); }
 * 
 * root.addContent(persSquare); }
 * 
 * 
 * /*********************************************************************** //2�
 * show the Xml of the agent.situation //ShowXml();
 * 
 * //3� Write the file WriteXml(directory+"/"+situationId+".xml");
 * 
 * 
 * }
 * 
 * /** Load the situXmlDb of an agent
 * 
 * @param directory
 *            the directory where are saved all the agents
 * @param agentName
 * @return the dbSituation
 * 
 *         public static SituationsDataBase ReadSituXmlDb(String
 *         directory,String agentName){
 * 
 *         String localisation; SituationsDataBase dbSituation;
 * 
 *         localisation=directory+"/"+agentName+"/situations/"; dbSituation=new
 *         SituationsDataBase();
 * 
 *         //1� get all the name of the file present in this directory String
 *         []temporaryListOfFileName=new File(localisation).list();
 *         List<String>listOfFileName=new ArrayList<String>();
 * 
 *         int j=0; while(j<temporaryListOfFileName.length){
 *         if(temporaryListOfFileName[j].endsWith(".xml")){
 *         listOfFileName.add(temporaryListOfFileName[j]); } j++; }
 * 
 * 
 * 
 *         //2� for each agent.situation, load it and add it to dbSituation int
 *         i=0; while(i<listOfFileName.size()){ Situation
 *         s=readSituXml(localisation,listOfFileName.get(i));
 *         dbSituation.add(s); i++; }
 * 
 *         return dbSituation; }
 * 
 * 
 *         /** Load a agent.situation encountered by an agent
 * 
 * @param directory
 *            the repository where the situations are saved
 * @param fileName
 *            the name of the xml agent.situation
 * @return the agent.situation as an object
 * 
 *         public static Situation readSituXml(String directory,String
 *         fileName){
 * 
 *         Situation s=new Situation();
 * 
 *         // We create a SAXBuilder instance SAXBuilder sxb = new SAXBuilder();
 *         try { // We create a new JDOM document with the xml target file as
 *         argument document = sxb.build(new File(directory+"/"+fileName)); }
 *         catch(Exception e){ e.printStackTrace(); System.exit(-1); }
 * 
 *         // We initialise the root with the document's root root =
 *         document.getRootElement();
 *         Debug.info("name of the root: "+root.getName(),1);
 * 
 *         Debug.info("Name of the agent.situation extracted from the tree: "+
 *         root.getChild("name").getText(), 1);
 *         s.setId(Integer.parseInt(root.getChild("name").getText()));
 * 
 *         Debug.info(
 *         "nbStep in the episode where this agent.situation was learned: "
 *         +root.getChildText("nbStep"),1);
 *         s.setNbStep(Integer.parseInt(root.getChildText("nbStep")));
 * 
 * 
 *         Debug.info("Reward : "+root.getChildText("reward"), 1);
 *         s.setRewardValue(Integer.parseInt(root.getChildText("reward")));
 * 
 *         Debug.info("vip: "+root.getChildText("vip"), 1);
 *         s.setVip(Integer.parseInt(root.getChildText("vip")));
 * 
 *         Debug.info("orientation: "+root.getChildText("orientation"),1);
 *         s.setOrientation
 *         (Orientation.valueOf(root.getChildText("orientation")));
 * 
 *         Debug.info("lastAction: "+root.getChildText("lastAction"), 1);
 *         s.setLastAction(Action.valueOf(root.getChildText("lastAction")));
 * 
 *         Element concepts = root.getChild("concepts");
 * 
 *         // get all the child of the concept node List
 *         listElem=concepts.getChildren(); Iterator iter=listElem.iterator();
 *         while(iter.hasNext()){ Element current=(Element)iter.next();
 *         Debug.info
 *         ("Attribute name : "+current.getName()+" concept : "+current
 *         .getText(),1); s.addConcept(current.getText()); }
 * 
 * 
 *         //Loading objects Element obj=root.getChild("objects");
 * 
 *         // get all the child of the object node List
 *         listObj=obj.getChildren(); iter=listObj.iterator();
 *         while(iter.hasNext()){ Element current=(Element)iter.next();
 * 
 *         Debug.info("attribute name : "+current.getName()+" content: "+current
 *         .getText(),1); s.addObject(ObjectType.valueOf(current.getName()));
 *         //,Class.forName(current.getText()).newInstance());
 * 
 *         }// endWhile on object
 * 
 * 
 *         Element caracs=root.getChild("caracteristics");
 * 
 *         //get all the child of the caracteristics node
 *         listElem=caracs.getChildren(); iter=listElem.iterator();
 * 
 *         Element temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.
 *         getName()+" content: "+temp.getText(),1);
 *         s.setAgentAltitude(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setMinAltitude(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setMaxAltitude(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setMeanAltitude(Float.parseFloat(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setExtent_fieldOfVision(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setTheoricalMaximum_visionLength(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setMaximum_visionLength(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setAltitudeHomogeneity(Float.parseFloat(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setMeanMct(Float.parseFloat(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setRoundAngle(Integer.parseInt(temp.getText()));
 * 
 *         temp=(Element)iter.next();
 *         Debug.info("attribute name: "+temp.getName(
 *         )+" content: "+temp.getText(),1);
 *         s.setRealAngle(Float.parseFloat(temp.getText()));
 * 
 * 
 *         /********************************************************************
 *         ********************
 * 
 *         //We create a list with all the persSquare node of the
 *         agent.situation List persSquareList = root.getChildren("persSquare");
 * 
 *         // We visit all square iter = persSquareList.iterator();
 *         while(iter.hasNext()){
 * 
 *         Element current = (Element)iter.next();
 * 
 *         // We get the current square Id int
 *         currentId=Integer.parseInt(current.getAttributeValue("id"));
 *         Debug.info("Current persSquare Id : "+currentId,1);
 * 
 *         int currentVisionLength=Integer.parseInt(current.getChildText(
 *         "visionLength"));
 *         Debug.info("Current visionLength : "+currentVisionLength,1);
 * 
 *         int currentSizeX=Integer.parseInt(current.getChildText("sizeX"));
 *         Debug.info("currentSizeX : "+currentVisionLength,1);
 * 
 *         int currentSizeY=Integer.parseInt(current.getChildText("sizeY"));
 *         Debug.info("currentSizeY: "+currentSizeY, 1);
 * 
 *         // We get the current square state boolean squareState =
 *         Boolean.parseBoolean(current.getChild("state").getText());
 *         Debug.info("Current square State (know ?): "+squareState,1);
 * 
 *         // We get the current square altitude int
 *         currentAltitude=Integer.parseInt
 *         (current.getChild("altitude").getText());
 *         Debug.info("Current square altitude : "+currentAltitude,1);
 * 
 *         //we get the mct of the square int
 *         mct=Integer.parseInt(current.getChildText("mct"));
 *         Debug.info("Current  Square mct:" + mct, 1);
 * 
 * 
 *         Square tempSq=new Square(currentId,currentAltitude);
 *         //topo.addSquare(temp); tempSq.setMct(mct);
 * 
 * 
 * 
 *         // MOVE TO THE PROPETIES NODE OF THE CURRENT SQUARE Element prop =
 *         current.getChild("properties");
 * 
 *         // get all the child of the properties node
 *         listElem=prop.getChildren(); Iterator iter1=listElem.iterator();
 *         while(iter1.hasNext()){ Element current1=(Element)iter1.next();
 * 
 *         Debug.info("Attribute name : "+current1.getName()+" content: "+
 *         current1.getText(),1);
 *         tempSq.setPropertyValue(PropertiesType.valueOf(current1.getName()),
 *         Float.parseFloat(current1.getText())); }
 * 
 * 
 *         // MOVE TO THE OBJECT NODE OF THE CURRENT SQUARE
 *         obj=current.getChild("object");
 * 
 *         // get all the child of the object node listObj=obj.getChildren();
 *         Iterator iter2=listObj.iterator(); while(iter2.hasNext()){ Element
 *         current2=(Element)iter2.next();
 * 
 *         Debug.info("attribute name : "+current2.getName()+" content: "+
 *         current2.getText(),1); try{
 *         tempSq.addObj(ObjectType.valueOf(current2.
 *         getName()),Class.forName(current2.getText()).newInstance());
 *         }catch(Exception e){
 *         System.out.println("Exception dans liaison xml nom/classe pour objets "
 *         ); e.printStackTrace(); System.exit(-1); }
 * 
 *         }// endWhile on object
 * 
 *         PersSquare persSq=new
 *         PersSquare(currentId,currentVisionLength,tempSq);
 *         s.addPersSquareToSituation(persSq);
 * 
 *         }//endWhile on square
 * 
 *         return s; }
 **/
