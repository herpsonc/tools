package fileManipulations;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;


/**
 * JSon Utils allows serialization/deserialization from/to Java to/form  JSON
 * 
 * 
 * @author Sylvain Ductor
 * @deprecated
 */
public class JSonUtil {

	public static <T> T fromFile(final String filename, final Class<T> prototype){
		try {
			return new ObjectMapper().readValue(new File(filename), prototype);
		} catch (final IOException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}


	public static <T> T fromFile(final String filename, final TypeReference<T> typeReference) {
		try {
			return new ObjectMapper().readValue(new File(filename), typeReference);
		} catch (final IOException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}

	public static <T> T fromString(final String jsonString, final Class<T> prototype){
		try {
			return new ObjectMapper().readValue(jsonString, prototype);
		} catch (final IOException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}


	public static <T> String toJSONString(final T object){
		try {
			return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (final IOException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}

	public static <T> void toFile(final T object, final String filename){
		try {
			new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(new File(filename), object);
		} catch (final IOException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}

	public static <T> JsonSchema toSchema(final Class<T> prototype){
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.generateJsonSchema(prototype);
			//			SchemaFactoryWrapper visitor = new SchemaFactoryWrapper();
			//			mapper.acceptJsonFormatVisitor(mapper.constructType(prototype), visitor);
			//			return visitor.finalSchema().toString();
		} catch (final JsonMappingException e) {
			throw new RuntimeException("error in json conversion",e);
		}
	}

	
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// TEST
	/////////////////////////////////////////////////////////////////////////////////////

	public static class Couple<A,B>{
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WRONG : Parameters will be lost during deserialization!!!!!!!!!!!!!!

	}

	public static class CoupleInteger2 {

		public Integer a;

		public Integer b;

		public CoupleInteger2(){

		}

		public CoupleInteger2(final Integer a, final Integer b) {
			this.a = a;
			this.b = b;
		}
	}
	
	public static class CoupleInteger {
		private Integer a;

		private Integer b;

		public CoupleInteger2 c;

		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Don't forget a dummy constructor
		public CoupleInteger(){

		}

		public CoupleInteger(final Integer a, final Integer b) {
			this.a = a;
			this.b = b;
			this.c = new CoupleInteger2(3,4);
		}

		/**
		 * @return the a
		 */
		public Integer getFirst() {
			return this.a;
		}

		/**
		 * @param a the a to set
		 */
		public void setFirst(final Integer a) {
			this.a = a;
		}

		/**
		 * @return the b
		 */
		public Integer getSecond() {
			return this.b;
		}

		/**
		 * @param b the b to set
		 */
		public void setSecond(final Integer b) {
			this.b = b;
		}
	}


	public static class test {
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Don't forget to put your field on public
		/////////////////// or to put them on private and define getters/setters!
		public String a;
		public int b;
		public String[] c;
		public List<Integer> d;
		public CoupleInteger e;


		public test(){
			this.a = "yo";
			this.b = 3;
			this.c = new String[]{"hu","ho","hoi"};
			this.d = Arrays.asList(new Integer[]{1,2,3});
			this.e = new CoupleInteger(4, 5);
		}

		@Override
		public String toString(){
			return "test in java yeah! "+this.a+" "+this.b+"  "+Arrays.asList(this.c)+" "+this.d+" "+this.e;
		}

	}

}


