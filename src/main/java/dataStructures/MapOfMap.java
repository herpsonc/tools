/**
 * 
 */
package dataStructures;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Map of Map class implemented using HashMap
 * @author hc
 * @param <K> Type of the first level Key
 * @param <K2> Type of the second level Key
 * @param <V> Type of the values in the second level Map
 *
 */
public class MapOfMap<K, K2, V> implements Map<K,Map<K2,V>>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7628106518548600420L;
	
	private Map<K,Map<K2,V>> mapofmap;

	/*************************************
	 *         
	 *           CONSTRUCTOR(S)
	 * 
	 ************************************/

	public MapOfMap(){
		mapofmap=new HashMap<K, Map<K2,V>>();
	}


	/*************************************
	 *         
	 *           METHOD(S)
	 * 
	 ************************************/


	/* (non-Javadoc)
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		this.mapofmap=null;

	}

	/**
	 * Not implemented
	 */
	@Override
	public boolean containsKey(Object key) {
		return false;
	}


	/**
	 * not implemented
	 */
	@Override
	public boolean containsValue(Object value) {
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<java.util.Map.Entry<K, Map<K2, V>>> entrySet() {

		return this.mapofmap.entrySet();
	}


	@Override
	public Map<K2, V> get(Object key) {	
		return this.mapofmap.get(key);
	}

	/**
	 * 
	 * @param key1
	 * @param key2
	 * @return null if one of the keys is not present in the Map
	 */
	public V get(K key1,K2 key2){
		Map<K2,V> subMap=this.mapofmap.get(key1);
		return (subMap!=null)? subMap.get(key2):null;	
	}


	/* (non-Javadoc)
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {	
		return this.mapofmap.isEmpty();
	}

	public boolean isEmpty(K key1){
		Map<K2,V> subMap=this.mapofmap.get(key1);
		return (subMap!=null)? subMap.isEmpty():true;
	}

	/**
	 * Return the first level k set
	 */
	@Override
	public Set<K> keySet() {
		return this.mapofmap.keySet();
	}

	/**
	 * Add the value V to the Map.
	 * WARNING - only use this method if the key and subkey are STRING otherwise use the 3 parameters put function.
	 * This method generates the subkey automatically adding a value at the end of the key given in parameter.
	 * @param key
	 * @param value
	 * @return the newly created subkey
	 */
	@SuppressWarnings("unchecked")
	public K2 add(K key,V value){
		Map<K2, V> submap=this.mapofmap.get(key);
		int idtemp=0;
		K2 k2=null;
		if (submap==null){
			submap=new HashMap<K2,V>();
			k2=(K2) (key+"-"+idtemp);
			submap.put(k2, value);
			this.mapofmap.put(key, submap);
		}else{
			if (submap.isEmpty()){
				k2=(K2) (key+"-"+idtemp);
			}else{
				Set<K2> listOfExistingKey2=submap.keySet();

				for (K2 k:listOfExistingKey2){
					Integer i=Integer.valueOf(((String)k).substring(((String) k).indexOf("-")+1));
					idtemp= (idtemp<i)?i:idtemp;
				}
				k2=(K2) (key+"-"+idtemp);
			}
			submap.put(k2, value);
		}
		
		return k2;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Map<K2, V> put(K key, Map<K2, V> value) {
		this.mapofmap.put(key, value);
		return value;
	}

	public void put(K key,K2 key2,V value){
		Map<K2, V> subMap=this.mapofmap.get(key);
		if (subMap==null){
			subMap=new HashMap<K2,V>();
		}
		subMap.put(key2, value);
	}


	/**
	 * not implemented
	 */
	@Override
	public void putAll(Map<? extends K, ? extends Map<K2, V>> m) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public Map<K2, V> remove(Object key) {
		Map<K2, V> subMap=this.mapofmap.get(key);
		this.mapofmap.remove(key);
		return subMap;
	}

	
	public V removeSubKey(K key,K2 key2) {
		Map<K2, V> subMap=this.mapofmap.get(key);
		return (subMap!=null) ? subMap.remove(key2) : null;
	}


	/**
	 * Size of the first level map
	 */
	@Override
	public int size() {
		return this.mapofmap.size();
	}

	/**
	 *
	 * @param key
	 * @return The size of one second level table
	 */
	public int size(K key){
		Map<K2, V> subMap=this.mapofmap.get(key);
		return (subMap!=null)? subMap.size():0;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#values()
	 */
	@Override
	public Collection<Map<K2, V>> values() {
		return this.mapofmap.values();
	}



}
