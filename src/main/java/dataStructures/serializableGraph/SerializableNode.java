package dataStructures.serializableGraph;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;


/**
 * A generic serializable node
 * 
 * @author hc
 *
 * @param <K> node ID type
 * @param <V> node content type (if any)
 */

public class SerializableNode< K,V>  implements Serializable, Cloneable, Comparable<K> {

	private static final long serialVersionUID = 1013050551956112154L;
	private K nodeID;
	private V value;

	/**
	 * Create node 
	 * @param nodeId
	 * @param nodeContent
	 */
	public SerializableNode(K nodeId,V nodeContent){
		this.nodeID=nodeId;
		this.value=nodeContent;
	}

	/**
	 * Create a node with no (null) content
	 * @param nodeId
	 */
	public SerializableNode(K nodeId){
		this.nodeID=nodeId;
		this.value=null;
	}


	public K getNodeId(){
		return this.nodeID;
	}

	public V getNodeContent(){
		return this.value;
	}

	public void setId(K nodeId){
		this.nodeID=nodeId;
	}

	public void setContent(V nodeContent){
		this.value=nodeContent;
		String s;
	}


	@Override
	public String toString(){
		return "("+nodeID.toString()+","+value.toString()+")";
	}

	public boolean equals(Object n) {
		return this.nodeID.toString().equalsIgnoreCase(((SerializableNode<K,V>)n).getNodeId().toString());
	}
	
	public int hashCode() {
		return this.getNodeId().hashCode();
	}

	@Override
	public int compareTo(K o) {
		return (o.toString().equalsIgnoreCase(this.nodeID.toString()))?0:1;
	}

	/**
	 * Returns a new instance of the node. 
	 * Only works if  K and V are String/Integer or possess a clone() method. 
	 */
	public SerializableNode<K,V> clone() throws CloneNotSupportedException{
		super.clone();
		K newId=null;
		V newVal=null;
		if (this.nodeID.getClass().getName().equalsIgnoreCase("String")||this.nodeID.getClass().getName().equalsIgnoreCase("Integer")){
			newId=this.nodeID;
		}else{
			try {
				newId= (K) this.nodeID.getClass().getMethod("clone").invoke(this.nodeID);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(this.value.getClass().getName().equalsIgnoreCase("String")||this.value.getClass().getName().equalsIgnoreCase("Integer")){
			newVal = this.value;
		}else{
			try {
				newVal = (V) this.value.getClass().getMethod("clone").invoke(this.value);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new SerializableNode<K, V>(newId,newVal);
	}


}
