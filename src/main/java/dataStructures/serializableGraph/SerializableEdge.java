package dataStructures.serializableGraph;

import java.io.Serializable;

/**
 * A generic serializable edge
 * 
 * @author hc
 *
 * @param <K> edge and node ID type
 * @param <V> edge content type (if any)
 */
public class SerializableEdge<K,V> implements Serializable {

	private static final long serialVersionUID = -1987810154047234879L;

	private K edgeID;
	private K node1Id;
	private K node2Id;
	private boolean directed;
	private V value;

	public SerializableEdge(K edgeId,K originNodeId,K destinationNodeId, boolean directed, V edgeContent){
		this.edgeID=edgeId;
		this.node1Id=originNodeId;
		this.node2Id=destinationNodeId;
		this.directed=directed;
		this.value=edgeContent;
	}

	/**
	 * Undirected simple edge
	 * @param edgeId
	 * @param originNodeId
	 * @param destinationNodeId
	 */
	public SerializableEdge(K edgeId,K originNodeId,K destinationNodeId){
		this.edgeID=edgeId;
		this.node1Id=originNodeId;
		this.node2Id=destinationNodeId;
		this.directed=false;
		this.value=null;
	}

	/**
	 * Set the content of a given edge (replace the previous one if any)
	 * @param edgeContent
	 */
	public void setEdgeContent(V edgeContent){
		this.value=edgeContent;
	}

	public String toString() {
		return "Edge "+this.edgeID.toString()+" [src: "+this.node1Id.toString()+" - dest:"+this.node2Id.toString()+"]- directed: "+this.directed+" - "+this.value.toString();  
	}

}
