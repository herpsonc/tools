/**
 * 
 */
package dataStructures;

import java.util.ArrayList;
import java.util.List;

import dataStructures.tuple.Couple;



/**
 * Class used to represent a function as a (sorted) list of points.
 * @author hc
 */
public class Curve {

	//keys behave like hashMap, and values like arrayList. See Guava's multimap
	private List<Couple<String,String>> points;
	private String unitX;
	private String unitY;
	private String name;

	/*************************************
	 *         
	 *           CONSTRUCTOR(S)
	 * 
	 ************************************/

	/**
	 * Create a empty curve and store the axis respective units.
	 * @param unitX
	 * @param unitY
	 * @param name legend
	 */
	public Curve(String unitX,String unitY,String name){
		this.points=new ArrayList<>();
		this.unitX=unitX;
		this.unitY=unitY;
		this.name=name;
	}

	/*************************************
	 *         
	 *           METHOD(S)
	 * 
	 ************************************/

	/**
	 * Add a point to the curve
	 * @param xValue
	 * @param yValue
	 */
	public void add(String xValue,String yValue){
		this.points.add(new Couple<String,String>(xValue, yValue));
	}



	/**
	 * Merge two curves into one. It requires they share the same units along their axis.
	 * @param c1 first curve
	 * @param c2 second curve
	 * @return the resulting curve sum of c1 and c2, Null if the units are different.
	 */
	public static Curve merge(Curve c1,Curve c2){
		Curve c3=null;
		if(c1==null){
			c3=(Curve) c2.clone();
		}else{
			if(c2==null){
				c3=(Curve) c1.clone();
			}else{
				//test units equivalence
				if (!c1.unitX.equals(c2.unitX) || !c1.unitY.equals(c2.unitY)){
					return null;
				}else{
					c3=new Curve(c1.getAxisXunit(), c1.getAxisYunit(),c1.name+" + "+c2.name);			
					List<Couple<String, String>> values1= c1.points;
					List<Couple<String, String>> values2= c2.points;
					int i1=0;
					int i2=0;
					while(i1 <values1.size() & i2<values2.size()){
						Long x1=Long.decode(values1.get(i1).getLeft());
						Long x2=Long.decode(values2.get(i2).getLeft());

						Integer res=Integer.valueOf(values1.get(i1).getRight())+Integer.valueOf(values2.get(i2).getRight());
						if (x1<x2){
							c3.add(x1.toString(),res.toString());
							i1++;
						}
						if (x1>x2){
							c3.add(x2.toString(), res.toString());
							i2++;
						}
						if(x1.equals(x2)){
							c3.add(x1.toString(), res.toString());
							i1++;
							i2++;
						}						
					}//end while

				}//end else
			}//end else

		}//end else

		return c3;
	}


	/*************************************
	 *         
	 *           GET and SET
	 * 
	 ************************************/

	public List<Couple<String,String>> getValues(){
		return this.points;
	}

	public void setValues(List<Couple<String, String>> values){
		this.points=values;
	}

	public String getAxisXunit(){
		return this.unitX;
	}

	public String getAxisYunit(){
		return this.unitY;
	}

	public String toString(){
		return "Unit on X: "+this.unitX+" - Unit on Y: "+this.unitY+"\n"+
				"Values: \n"+this.points.toString();
	}

	@Override
	public Object clone(){
		Curve c=new Curve(unitX, unitY,name);
		for (Couple<String,String> cpl :this.points){
			c.add(cpl.getLeft(),cpl.getRight());
		}
		return c;
	}

	public void show(){

	}
	
	public String getname(){
		return this.name;
	}


}
