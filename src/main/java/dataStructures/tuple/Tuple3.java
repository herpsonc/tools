
package dataStructures.tuple;

import java.io.Serializable;

import def.HashCodeUtil;


/**
 * 
 * @author Sylvain Ductor
 */

public class Tuple3<A, B, C> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1153604584449397451L;
	private final A a;
	private final B b;
	private final C c;

	/*************************************
	 * 
	 * CONSTRUCTOR(S)
	 * 
	 ************************************/

	public Tuple3(final A a, final B b, final C c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	/*************************************
	 * 
	 * METHOD(S)
	 * 
	 ************************************/

	/*************************************
	 * 
	 * GET and SET
	 * 
	 ************************************/

	public A getFirst() {
		return this.a;
	}

	public B getSecond() {
		return this.b;
	}

	public C getThird() {
		return this.c;
	}

	/*************************************
	 * 
	 * Primitives
	 * 
	 ************************************/

	@Override
	public String toString() {
		return "(" + this.a.toString() + ", " + this.b.toString() + ", "
				+ this.c.toString() + ")";
	}

	@Override
	public boolean equals(final Object that) {
		if (that instanceof Tuple3
				&& ((Tuple3) that).getFirst().getClass()
				.equals(this.a.getClass())
				&& ((Tuple3) that).getSecond().getClass()
				.equals(this.b.getClass())
				&& ((Tuple3) that).getThird().getClass()
				.equals(this.c.getClass())) {
			final Tuple3<A, B, C> t = (Tuple3) that;
			return this.a.equals(t.a) && this.b.equals(t.b)
					&& this.c.equals(t.c);
		} else {
			return false;
		}

	}

	@Override
	public int hashCode() {
		int result = HashCodeUtil.SEED;
		// collect the contributions of various fields
		result = HashCodeUtil.hash(result, this.a);
		result = HashCodeUtil.hash(result, this.b);
		result = HashCodeUtil.hash(result, this.c);
		return result;
	}
}