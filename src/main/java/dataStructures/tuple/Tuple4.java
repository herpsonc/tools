package dataStructures.tuple;

import java.io.Serializable;

import def.HashCodeUtil;

/**
 * 
 * @author Sylvain Ductor
 */

public class Tuple4<A,B,C,D>  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7421799553778796386L;
	
	private final A _1;
	private final B _2;
	private final C _3;
	private final D _4;
	/**
	 * @param _1
	 * @param _2
	 * @param _3
	 * @param _4
	 */
	public Tuple4(A _1, B _2, C _3, D _4) {
		super();
		this._1 = _1;
		this._2 = _2;
		this._3 = _3;
		this._4 = _4;
	}
	/**
	 * @return the _1
	 */
	public A get_1() {
		return _1;
	}
	/**
	 * @return the _2
	 */
	public B get_2() {
		return _2;
	}
	/**
	 * @return the _3
	 */
	public C get_3() {
		return _3;
	}
	/**
	 * @return the _4
	 */
	public D get_4() {
		return _4;
	}

	/*************************************
	 * 
	 * Primitives
	 * 
	 ************************************/

	@Override
	public String toString() {
		return "(" + this._1.toString() + ", " + this._2.toString() + ", "
				+ this._3.toString() + ","+ this._4.toString()+ ")";
	}

	@Override
	public boolean equals(final Object that) {
		if (that instanceof Tuple4
				&& ((Tuple4) that).get_1().getClass()
				.equals(this._1.getClass())
				&& ((Tuple4) that).get_2().getClass()
				.equals(this._2.getClass())
				&& ((Tuple4) that).get_3().getClass()
				.equals(this._3.getClass())
				&& ((Tuple4) that).get_4().getClass()
				.equals(this._4.getClass())) {
			final Tuple4<A, B, C,D> t = (Tuple4) that;
			return this._1.equals(t._1) && this._2.equals(t._2)
					&& this._3.equals(t._3)&& this._4.equals(t._4);
		} else {
			return false;
		}

	}

	@Override
	public int hashCode() {
		int result = HashCodeUtil.SEED;
		// collect the contributions of various fields
		result = HashCodeUtil.hash(result, this._1);
		result = HashCodeUtil.hash(result, this._2);
		result = HashCodeUtil.hash(result, this._3);
		result = HashCodeUtil.hash(result, this._4);
		return result;
	}
	

}
