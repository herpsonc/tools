
package dataStructures.tuple;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import def.HashCodeUtil;

/**
 *  
 * @param <L> Left attribute of the couple. Should be serializable
 * @param <R> Right attribute of the couple. Should be serializable
 * 
 * @author Cédric Herpson
 */


public class Couple<L, R> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3394386018697742926L;
	private L a;
	private R b;

	/*************************************
	 * 
	 * CONSTRUCTOR(S)
	 * 
	 ************************************/

	public Couple(final L a, final R b) {
		this.a = a;
		this.b = b;
	}

	/*************************************
	 * 
	 * METHOD(S)
	 * 
	 ************************************/

	public static <A, B> Map<A, B> toMap(final Collection<Couple<A, B>> l) {
		final Map<A, B> result = new HashMap<A, B>();
		for (final Couple<A, B> c : l) {
			result.put(c.getLeft(), c.getRight());
		}
		return result;
	}

	/*************************************
	 * 
	 * GET and SET
	 * 
	 ************************************/

	/**
	 * 
	 * @return the left part of the couple
	 */
	public L getLeft() {
		return this.a;
	}

	/**
	 * 
	 * @return the right part of the couple
	 */
	public R getRight() {
		return this.b;
	}

	/*************************************
	 * 
	 * Primitives
	 * 
	 ************************************/

	@Override
	public String toString() {
		return "<" + ((a!=null)?this.a.toString():"") + ", " + ((b!=null)?this.b.toString():"") + ">";
	}

	@Override
	public boolean equals(final Object that) {
		if (that instanceof Couple
				&& ((Couple) that).getLeft().getClass()
				.equals(this.a.getClass())
				&& ((Couple) that).getRight().getClass()
				.equals(this.b.getClass())) {
			final Couple<L, R> t = (Couple<L, R>) that;
			return this.a.equals(t.a) && this.b.equals(t.b);
		} else {
			return false;
		}

	}

	@Override
	public int hashCode() {
		int result = HashCodeUtil.SEED;
		
		// collect the contributions of various fields
		result = HashCodeUtil.hash(result, this.a);
		result = HashCodeUtil.hash(result, this.b);
		return result;
	}
}
