package GUIgraphs;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYSeriesLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.chart.renderer.xy.XYDifferenceRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;

import dataStructures.Curve;
import dataStructures.tuple.Couple;

/**
 * Class used to display charts
 * Currently 4 chart types available :
 *   - ScatterPLot,
 *   - Difference Chart, 
 *   - TimeSerie
 *   - Fixed uncertainty on a time serie
 *   
 *   See users/cedric/plot/ for illustrative examples
 * @author Cédric Herpson
 *
 */
public class GraphGeneration extends ApplicationFrame {

	private static final long serialVersionUID = 42154466187670835L;


	public enum DiagramType{
		ScatterPlot,LineChart,DifferenceChart,TimeSerie,TimeSerieUncertainty
	}


	/**
	 * create the Frame containing the chart.
	 *
	 * @param title  the frame title.
	 * @param series the list of elements to use to create the chart
	 * @param diagType 
	 * @param uncertaintyValue uncertainty interval, if any (used for TimeSerieUncertainty)
	 */

	public GraphGeneration(String title,List<Curve> series,
			DiagramType diagType,String graphTitle,Double uncertaintyValue) {
		super(title);
		ChartPanel chartPanel = (ChartPanel) createDemoPanel(series,diagType,graphTitle,uncertaintyValue);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		setContentPane(chartPanel);
	}

	/**
	 * Creates a panel
	 * @param series the list of curves to add
	 * @param diagType the type of graphic
	 * @param uncertaintyValue uncertainty interval, if any (used for TimeSerieUncertainty)
	 * @return A panel.
	 */
	private static JPanel createDemoPanel(List<Curve> series,DiagramType diagType,String graphTitle,Double uncertaintyValue) {

		boolean interval= DiagramType.TimeSerieUncertainty.equals(diagType)?true:false;

		JFreeChart chart = createChart(createDataset(series,interval,uncertaintyValue),diagType,graphTitle,series.get(0).getAxisXunit(),series.get(0).getAxisYunit());
		ChartPanel panel = new ChartPanel(chart);
		panel.setFillZoomRectangle(true);
		panel.setMouseWheelEnabled(true);

		//ChartRenderingInfo cri=new ChartRenderingInfo();
		//chart.draw(new Graphics2D(), new Rectangle2D(), cri);

		//try {
		//	String fname=ServletUtilities.saveChartAsPNG(chart,1109 ,556,,null);
		//	System.out.println("fichier resultat : "+fname);
		//	
		//} catch (IOException e) {
		//	
		//	e.printStackTrace();
		//}
		//		File fileName = new File(System.getProperty("user.home") + "/jfreechart1.pdf");
		//		saveChartAsPDF(fileName, chart, 400, 300, new DefaultFontMapper());
		//		
		return panel;
	}

	/**
	 * Creates a chart IPC/IDH from the dataSet.
	 *
	 * @param dataset
	 *
	 * @return A chart.
	 */
	private static JFreeChart createChart(XYDataset dataset,DiagramType diagType,String graphTitle,String labelX, String labelY) {


		JFreeChart chart=null;
		switch(diagType){
		case DifferenceChart:
			chart=ChartFactory.createTimeSeriesChart(graphTitle, labelX, labelY, dataset, true, true, false);

			XYPlot xyplot = (XYPlot)chart.getPlot();
			xyplot.setDomainPannable(true);
			XYDifferenceRenderer xydifferencerenderer = new XYDifferenceRenderer(Color.green, Color.red, false);
			xydifferencerenderer.setRoundXCoordinates(true);
			xyplot.setDomainCrosshairLockedOnData(true);
			xyplot.setRangeCrosshairLockedOnData(true);
			xyplot.setDomainCrosshairVisible(true);
			xyplot.setRangeCrosshairVisible(true);
			xyplot.setRenderer(xydifferencerenderer);
			DateAxis dateaxis = new DateAxis("Time");
			dateaxis.setLowerMargin(0.0D);
			dateaxis.setUpperMargin(0.0D);
			xyplot.setDomainAxis(dateaxis);
			xyplot.setForegroundAlpha(0.5F);
			ChartUtilities.applyCurrentTheme(chart);

			break;
		case LineChart:
			break;
		case ScatterPlot:
			chart= ChartFactory.createScatterPlot(	
					graphTitle,  // title
					labelX, //xAxisLabel
					labelY, // yAxisLabel
					dataset, //dataset
					PlotOrientation.VERTICAL,// orientation
					false, // legend
					true, //tooltips
					false //urls)
					);

			chart.setBackgroundPaint(Color.white);

			XYPlot plot = (XYPlot) chart.getPlot();
			plot.setBackgroundPaint(Color.lightGray);
			plot.setDomainGridlinePaint(Color.white);
			plot.setRangeGridlinePaint(Color.white);
			plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
			plot.setDomainCrosshairVisible(true);
			plot.setRangeCrosshairVisible(true);

			XYItemRenderer r = plot.getRenderer();	
			r.setLegendItemLabelGenerator(new StandardXYSeriesLabelGenerator());

			break;
		case TimeSerie:
			chart=ChartFactory.createTimeSeriesChart(graphTitle, labelX, labelY, dataset, true, true, false);

			XYPlot xyplot2 = (XYPlot)chart.getPlot();
			//xyplot2.setDomainPannable(true);
			//xyplot2.setRangePannable(false);
			//xyplot2.setDomainCrosshairVisible(true);
			//xyplot2.setRangeCrosshairVisible(true);
			org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot2.getRenderer();
			if (xyitemrenderer instanceof XYLineAndShapeRenderer)
			{
				XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyitemrenderer;
				xylineandshaperenderer.setBaseShapesVisible(false);
			}
			//			DateAxis dateaxis2 = (DateAxis)xyplot2.getDomainAxis();
			//			dateaxis2.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

			break;
		case TimeSerieUncertainty:
			chart=ChartFactory.createXYLineChart(graphTitle, labelX, labelY, dataset,  PlotOrientation.VERTICAL,true, true, false);

			XYPlot xyplot3 = (XYPlot)chart.getPlot();
			xyplot3.setDomainPannable(true);
			DeviationRenderer deviationrenderer = new DeviationRenderer(true, false);
			deviationrenderer.setSeriesStroke(0, new BasicStroke(3F, 1, 1));
			deviationrenderer.setSeriesStroke(0, new BasicStroke(3F, 1, 1));
			deviationrenderer.setSeriesStroke(1, new BasicStroke(3F, 1, 1));
			deviationrenderer.setSeriesFillPaint(0, new Color(255, 200, 200));
			deviationrenderer.setSeriesFillPaint(1, new Color(200, 200, 255));
			xyplot3.setRenderer(deviationrenderer);
			NumberAxis numberaxis = (NumberAxis)xyplot3.getRangeAxis();
			numberaxis.setAutoRangeIncludesZero(false);
			numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

			break;
		default:
			break;

		}



		return chart;

	}


	/**
	 * create a dataset from the series List
	 * @param series
	 * @param uncertainty true if a range of uncertainty has to be inserted, false otherwise.
	 * @param uncertaintyValue (used only if uncertainty is true)
	 * @return a dataset
	 */
	private static XYDataset createDataset(List<Curve> series,boolean uncertainty,Double uncertaintyValue) {
		XYSeriesCollection dataset=null;
		YIntervalSeriesCollection yintervalseriescollection=null;

		if (!uncertainty){
			dataset= new XYSeriesCollection();
			XYSeries o;

			Iterator<Curve> iter=series.iterator();
			//loop over the curves
			while (iter.hasNext()){
				Curve c=iter.next();
				Iterator<Couple<String, String>> iter2 =c.getValues().iterator();
				o= new XYSeries(c.getname());
				//loop over the values of a given function
				while(iter2.hasNext()){
					Couple<String, String> val=iter2.next();
					o.add(Double.valueOf(val.getLeft()),Double.valueOf(val.getRight()));
				}
				dataset.addSeries(o);
			}
		}else{//uncertainty interval
			yintervalseriescollection = new YIntervalSeriesCollection();
			YIntervalSeries yintervalseries;

			Iterator<Curve> iter=series.iterator();
			//loop over the curves
			while (iter.hasNext()){
				Curve c=iter.next();
				Iterator<Couple<String, String>> iter2 =c.getValues().iterator();
				yintervalseries= new YIntervalSeries(c.getname());
				//loop over the values of a given function
				while(iter2.hasNext()){
					Couple<String, String> val=iter2.next();
					yintervalseries.add(
							Double.valueOf(val.getLeft()),
							Double.valueOf(val.getRight()),
							Double.valueOf(val.getRight())-uncertaintyValue*Double.valueOf(val.getRight()),
							Double.valueOf(val.getRight())+uncertaintyValue*Double.valueOf(val.getRight()));
				}
				yintervalseriescollection.addSeries(yintervalseries);
			}	
		}

		return uncertainty?yintervalseriescollection:dataset;
	}



	/**
	 * Creates the graph and display it
	 *
	 * @param series the list of functions to display
	 * @param frameTitle 
	 * @param graphTitle
	 * @param diagType the type of graphic
	 * @param uncertaintyValue uncertainty interval in % (between 0 and 1), if any. Only used for used for TimeSerieUncertainty diagrams
	 * 
	 */
	public static void createGraph(List<Curve> series,String frameTitle,DiagramType diagType,String graphTitle,Double uncertaintyValue){

		GraphGeneration demo = new GraphGeneration(frameTitle,series, diagType, graphTitle,uncertaintyValue);
		demo.pack();
		RefineryUtilities.centerFrameOnScreen(demo);
		demo.setVisible(true);

	}

}



