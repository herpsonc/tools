package fileManipulations;

import fileManipulations.JSonUtil.test;

public class TestfileManipulation {

	public static void main(final String[] args){

		final String jsonString = JSonUtil.toJSONString(new test());
		System.out.println("---------------------");
		System.out.println(jsonString);
		System.out.println("---------------------");
		System.out.println(JSonUtil.fromString(jsonString, test.class));
		System.out.println("---------------------");
		System.out.println(JSonUtil.toSchema(test.class));
		System.out.println("---------------------");
		System.out.println(JSonUtil.toJSONString(JSonUtil.toSchema(test.class)));

	}
	
}
