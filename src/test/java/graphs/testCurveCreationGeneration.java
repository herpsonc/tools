/**
 * 
 */
package graphs;

import java.util.ArrayList;

import java.util.List;
import java.util.Random;

import GUIgraphs.GraphGeneration;
import dataStructures.Curve;

/**
 * Class used to illustrate the GraphGeneration class
 * 
 * @author Cédric Herpson
 *
 */
public class testCurveCreationGeneration {

	private static Curve c1;
	private static Curve c2;
	
	
	/*************************************
	 *         
	 *           CONSTRUCTOR(S)
	 * 
	 ************************************/
	
	

	/*************************************
	 *         
	 *           METHOD(S)
	 * 
	 ************************************/
	

	/*************************************
	 *         
	 *           GET and SET
	 * 
	 ***********************************/
	
	
	public static void main(String[]args){
		
		//1) Two data streams
		c1 = new Curve("milliseconds", "Watts", "House1");
		c2= new Curve("milliseconds", "Watts", "House2");
		
		
		//2) Random values are added to each stream
		Random r= new Random();
		
		//96 times 15 minutes in 24h
		int time=0;
		for (int i=0;i<96;i++){
			//cal.roll(Calendar.MINUTE, 15);
			//Long.toString(cal.getTimeInMillis())
			//Long.toString(cal.getTimeInMillis()+1000)
			c1.add(Integer.toString(time), Integer.toString(r.nextInt(20)));
			c2.add(Integer.toString(time+1), Integer.toString(r.nextInt(100)));
			time+=15;
		}
		
		Curve result=Curve.merge(c1, c2);
		
		
		System.out.println(c1.toString());
		System.out.println(c2.toString());
		System.out.println(result.toString());
		
		List<Curve> l= new ArrayList<Curve>();
		l.add(c1);
		l.add(c2);
		//l.add(result);
		
		//3) Graph creation using the various randering possibilities 
		GraphGeneration.createGraph(l, "test Difference Chart", GraphGeneration.DiagramType.DifferenceChart, "Title",null);
		GraphGeneration.createGraph(l, "test Time Serie", GraphGeneration.DiagramType.TimeSerie, "Title",null);
		GraphGeneration.createGraph(l, "test Scatter plot", GraphGeneration.DiagramType.ScatterPlot, "Title",null);
		GraphGeneration.createGraph(l, "test Uncertainty Deviation serie", GraphGeneration.DiagramType.TimeSerieUncertainty, "Title",0.1);
	}
	
}
